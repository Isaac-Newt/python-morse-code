# Python Morse Code transcriber

GTK3 Frontend to a Morse Code transcriber in Python3

## About

This is a morse code transcriber built using Python 3, with a GTK3 UI using Python-GObject bindings.


## Requirements

- Python >= 3.5 (Tested with 3.7.3)

- GTK+3 >= 3.22 (i.e. Ubuntu >= 18.04, Debian >= 9, openSUSE >= 15.0)

- PyGObject (i.e. Ubuntu: python3-gi, openSUSE: python3-gobject)


## License

This project is licensed under the Mozilla Public Licence v. 2.0.
